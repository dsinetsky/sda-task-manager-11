package ru.t1.dsinetsky.tm.model;

import java.util.UUID;

public final class Project {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String desc = "";

    public Project() {
    }

    public Project(String name) {
        this.name = name;
    }

    public Project(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new StringBuilder(name).append(" : ").append(desc).toString();
    }
}
