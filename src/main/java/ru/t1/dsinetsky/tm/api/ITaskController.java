package ru.t1.dsinetsky.tm.api;

public interface ITaskController {

    void showTasks();

    void createTask();

    void clearTasks();

    void showTaskById();

    void showTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void removeTaskById();

    void removeTaskByIndex();

    void createTestTasks();

}
