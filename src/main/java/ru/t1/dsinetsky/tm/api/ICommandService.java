package ru.t1.dsinetsky.tm.api;

import ru.t1.dsinetsky.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
