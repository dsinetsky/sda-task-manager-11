package ru.t1.dsinetsky.tm.service;

import ru.t1.dsinetsky.tm.api.ICommandRepository;
import ru.t1.dsinetsky.tm.api.ICommandService;
import ru.t1.dsinetsky.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }
    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
